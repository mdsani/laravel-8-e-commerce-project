<?php

namespace Database\Seeders;

use App\Models\Size;
use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Size::create([
            'title' => 'M',
            'description' => 'M'
        ]);
        Size::create([
            'title' => 'L',
            'description' => 'L'
        ]);
        Size::create([
            'title' => 'XL',
            'description' => 'XL'
        ]);
        Size::create([
            'title' => 'XXL',
            'description' => 'XXL'
        ]);
        Size::create([
            'title' => '3XL',
            'description' => '3XL'
        ]);
    }
}
