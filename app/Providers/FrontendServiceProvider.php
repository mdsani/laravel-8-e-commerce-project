<?php

namespace App\Providers;

use App\Http\Controllers\CommentController;
use App\View\Composers\FrontendComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class FrontendServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['components.backend.layouts.partials.top_bar'], CommentController::class);
        View::composer(['components.frontend.layouts.partials.header','components.frontend.layouts.partials.headerlink'], FrontendComposer::class);
    }
}
