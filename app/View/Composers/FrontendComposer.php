<?php
 
namespace App\View\Composers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;
 
class FrontendComposer
{
    protected $categories;
 
    public function __construct()
    {
        $this->categories = $this->getCategories();
    }
 
    public function setCategories()
    {
        return $categories = Category::orderBY('name','desc')->get();
    }

    public function getCategories()
    {
        return $categories = $this->setCategories();
    }

    public function compose(View $view)
    {
        $cartItem = Cart::get();
        $shoopingCartCount =count($cartItem) ?? 0;

        $wishlistItem = Wishlist::get();
        $shoopingWishCount =count($wishlistItem) ?? 0;
        
        $view->with([
            'shoopingCartCount' => $shoopingCartCount,
            'shoopingWishCount' => $shoopingWishCount,
            'categories' => $this->categories,
        ]);
    }
}