<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Image;

class ProfileController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        return view('backend.users.profile-edit', compact('user'));    
    }

    public function update(Request $request, Profile $profile)
    {
        // dd($request->all());
        // $profileData = $request->except('name','email','_token', '_method');
        

        auth()->user()->update(['name'=>$request->name,'email'=>$request->email]);
        $requestData = [
            'mobile_number' => $request->mobile_number,
            'country' => $request->country,
            'address' => $request->address,
            'bio' => $request->bio,
            'city' => $request->city,
            'area' => $request->area,
            'postcode' => $request->postcode,
        ];
        
        if($request->hasFile('image')){
            $request->validate([
                'image' => 'required|image|mimes:png,jpg',
            ]);
            $this->unlinkImage($profile->image);
            $requestData['image'] =$this->uploadImage(request()->file('image'), 'one');
        }

        auth()->user()->profile()->update($requestData);
        return redirect()->back()->with('status','Profile Updated Successfully !');
    }

    public function uploadImage($file)
    {
        $fileName =time().'.'.$file->getClientOriginalExtension();
        Image::make($file)->resize(600, 600)->save(storage_path().'/app/public/images/profiles/'.$fileName);
        return $fileName;
        
    }

    public function unlinkImage($fileName)
    {
        if(file_exists(storage_path().'/app/public/images/profiles/'.$fileName && !is_null($fileName))){
            unlink(storage_path().'/app/public/images/profiles/'.$fileName);
        }
    }
}
