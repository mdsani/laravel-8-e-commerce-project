<?php

namespace App\Http\Controllers;

use App\Exports\TagsExport;
use App\Http\Requests\TagRequest;
use App\Models\Tag;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class TagController extends Controller
{
    public function index()
    {
        try{
            $tagCollection = Tag::latest();
            if(request('search')){
                $tag = $tagCollection
                                ->where('title','like', '%'.request('search').'%');
            }
                $tag = $tagCollection->paginate(5);
                return view('backend.tags.index',[
                'tags' => $tag,
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.tags.create');
    }

    public function store(TagRequest $request)
    {
        try{
            Tag::create([
                'title' => $request->title
            ]);
            return redirect()->route('tags.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Tag $tag)
    {
        return view('backend.tags.show',[
            'tag' => $tag
        ]);
    }

    public function edit(Tag $tag)
    {
        return view('backend.tags.edit',[
            'tag' =>$tag
        ]);
    }

    public function update(TagRequest $request ,Tag $tag)
    {
        try{
            $tag->update([
                'title' => $request->title
            ]);
            return redirect()->route('tags.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Tag $tag)
    {
        try{
            $tag->delete();
            return redirect()->route('tags.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $tag = Tag::onlyTrashed()->get();
            return view('backend.tags.trash',[
                'tags' => $tag
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $tag = Tag::onlyTrashed()->findOrFail($id);
            $tag->restore();
            return redirect()->route('tags.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $tag = Tag::onlyTrashed()->findOrFail($id);
            $tag->forceDelete();
            return redirect()->route('tags.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function excel() 
    {
        return Excel::download(new TagsExport, 'tag.xlsx');
    }

    public function pdf()
    {
        $tag = Tag::all();
        $pdf = PDF::loadView('backend.tags.pdf', ['tags' => $tag]);
        return $pdf->download('tags.pdf');
    }
}
