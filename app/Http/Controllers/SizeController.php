<?php

namespace App\Http\Controllers;

use App\Exports\SizesExport;
use App\Models\Size;
use App\Http\Requests\StoreSizeRequest;
use App\Http\Requests\UpdateSizeRequest;
use Illuminate\Database\QueryException;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class SizeController extends Controller
{

    public function index()
    {
        try{
            $sizeCollection = Size::latest();
            if(request('search')){
                $size = $sizeCollection
                                ->where('title','like', '%'.request('search').'%')
                                ->orWhere('description', 'like', '%'.request('search').'%');
            }
                $size = $sizeCollection->paginate(5);
                return view('backend.sizes.index',[
                'sizes' => $size,
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.sizes.create');
    }

    public function store(StoreSizeRequest $request)
    {
        try{
            Size::create([
                'title' => $request->title,
                'description' =>$request->description
            ]);
            return redirect()->route('sizes.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Size $size)
    {
        return view('backend.sizes.show',[
            'size' => $size
        ]);
    }

    public function edit(Size $size)
    {
        return view('backend.sizes.edit',[
            'size' =>$size
        ]);
    }

    public function update(UpdateSizeRequest $request, Size $size)
    {
        try{
            $size->update([
                'title' => $request->title,
                'description' => $request->description
            ]);
            return redirect()->route('sizes.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Size $size)
    {
        try{
            $size->delete();
            return redirect()->route('sizes.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $size = Size::onlyTrashed()->get();
            return view('backend.sizes.trash',[
                'sizes' => $size
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $size = Size::onlyTrashed()->findOrFail($id);
            $size->restore();
            return redirect()->route('sizes.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $size = Size::onlyTrashed()->findOrFail($id);
            $size->forceDelete();
            return redirect()->route('sizes.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function excel() 
    {
        return Excel::download(new SizesExport, 'size.xlsx');
    }

    public function pdf()
    {
        $size = Size::all();
        $pdf = PDF::loadView('backend.sizes.pdf', ['sizes' => $size]);
        return $pdf->download('sizes.pdf');
    }
}
