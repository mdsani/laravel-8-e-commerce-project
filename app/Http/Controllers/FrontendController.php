<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Comment;
use App\Models\Order;
use App\Models\Product;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;

class FrontendController extends Controller
{
    public function index()
    {
        // $bestsellingproducts = Product::where('featured',0)->orderBY('best_selling', 'desc')->limit(8)->get();
        // $ourfeaturedproducts = Product::where('featured',1)->limit(4)->get();
        $mobiles = Product::where('category_id',1)->latest()->limit(4)->get();
        $mobiles_accessories = Product::where('category_id',3)->where('subcategory_id',7)->latest()->limit(4)->get();
        $smart_watch = Product::where('category_id',3)->where('subcategory_id',6)->limit(4)->latest()->get();
        $computer_accesories = Product::where('category_id',4)->limit(4)->latest()->get();
        $life_style = Product::where('category_id',5)->limit(4)->latest()->get();
        $electronics_appliances = Product::where('category_id',2)->limit(4)->latest()->get();
        return view('frontend.home', compact('mobiles','mobiles_accessories','smart_watch','computer_accesories','life_style','electronics_appliances'));
    }

    public function filter()
    {
        $categories = Category::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $tags = Tag::orderBY('title','asc')
                            ->pluck('title','id')
                            ->toArray();
        $sizes = Size::orderBY('title','asc')
                            ->pluck('title','id')
                            ->toArray();
        $colors = Color::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $brands = Brand::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $products = Product::where('featured',0)->paginate(20);
        return view('frontend.shop-classic-filter', compact('products','categories','tags','sizes','colors','brands'));
    }

    public function singleProduct(Product $product)
    {
        $productrelated = Product::where('subcategory_id', $product->subcategory->id)->limit(4)->get();

        $comments = $product->comments()->paginate(4);

        $comment = Comment::get();
        $commentCount =count($comment) ?? 0;

        return view('frontend.product-details',compact('product','productrelated','comments','commentCount'));
    }

    public function show($subcategoryId)
    {
        $products = Product::where('subcategory_id', $subcategoryId)->get();
        // dd($products->all());
        // $products = $products->products()->paginate(20);
        return view('frontend.shop', compact('products'));
    }

    public function allshow($categoryId)
    {
        $products = Product::where('category_id', $categoryId)->get();
        return view('frontend.shop', compact('products'));
    }

    public function subshow($categoryId)
    {
        $products = Product::where('subcategory_id', $categoryId)->get();
        return view('frontend.shop', compact('products'));
    }

    public function myOrder()
    {
        $orders = Order::where('user_id',Auth::id())->get();
        return view('frontend.myorders', compact('orders'));
    }

    public function viewOrder($id)
    {
        $orders = Order::where('id',$id)->where('user_id',Auth::id())->first();
        return view('frontend.vieworders', compact('orders'));
    }

    public function edit()
    {
        $user = auth()->user();
        return view('frontend.profile', compact('user'));  
    }

}
