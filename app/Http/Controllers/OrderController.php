<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::where('status','0')->Orwhere('status','1')->get();
        return view('backend.orders.index', compact('orders'));
    }

    public function show($id)
    {
        $orders = Order::where('id',$id)->first();
        return view('backend.orders.show', compact('orders'));
    }

    public function update(Request $request,$id)
    {
        $order = Order::find($id);
        $order->update([
            'status' => $request->status
        ]);
        return redirect()->route('orders.index')->withMessage('Task was successful!');
    }

    public function history()
    {
        $orders = Order::Orwhere('status','2')->get();
        return view('backend.orders.history', compact('orders'));
    }

    public function cancel()
    {
        $orders = Order::Orwhere('status','3')->get();
        return view('backend.orders.order-cancel', compact('orders'));
    }

    public function remove($id)
    {
        $order = Order::findOrFail($id);
        try{         
        $order->update([
            'status' => 3,
        ]);
            return redirect()->back()->with('status','Order Cancel!');

        }catch(QueryException $e){
            dd($e->getMessage());
            echo $e->getMessage();
        }
    }
}
