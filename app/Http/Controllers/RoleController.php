<?php

namespace App\Http\Controllers;

use App\Exports\RolesExport;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Excel;
use PDF;

class RoleController extends Controller
{
    public function index()
    {
        try{
            $roles = Role::get();
            return view('backend.roles.index', [
            'roles' => $roles
        ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function show(Role $role)
    {
        // $users = $role->users;
        return view('backend.roles.show',compact('role'));
    }

    public function excel() 
    {
        return Excel::download(new RolesExport, 'roles.xlsx');
    }

    public function pdf(Role $role)
    {
        $pdf = PDF::loadView('backend.roles.pdf', compact('role'));
        return $pdf->download('roles.pdf');
    }
}
