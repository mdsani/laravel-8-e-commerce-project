<?php

namespace App\Http\Controllers;

use App\Exports\ColorsExport;
use App\Models\Color;
use App\Http\Requests\StoreColorRequest;
use App\Http\Requests\UpdateColorRequest;
use Illuminate\Database\QueryException;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class ColorController extends Controller
{
    public function index()
    {
        try{
            $colorCollection = Color::latest();
            if(request('search')){
                $color = $colorCollection
                                ->where('name','like', '%'.request('search').'%');
            }
                $color = $colorCollection->paginate(5);
                return view('backend.colors.index',[
                'colors' => $color,
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.colors.create');
    }

    public function store(StoreColorRequest $request)
    {
        try{
            Color::create([
                'name' => $request->name
            ]);
            return redirect()->route('colors.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Color $color)
    {
        return view('backend.colors.show',[
            'color' => $color
        ]);
    }

    public function edit(Color $color)
    {
        return view('backend.colors.edit',[
            'color' =>$color
        ]);
    }

    public function update(UpdateColorRequest $request, Color $color)
    {
        try{
            $color->update([
                'name' => $request->name
            ]);
            return redirect()->route('colors.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Color $color)
    {
        try{
            $color->delete();
            return redirect()->route('colors.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $color = Color::onlyTrashed()->get();
            return view('backend.colors.trash',[
                'colors' => $color
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $color = Color::onlyTrashed()->findOrFail($id);
            $color->restore();
            return redirect()->route('colors.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $color = Color::onlyTrashed()->findOrFail($id);
            $color->forceDelete();
            return redirect()->route('colors.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function excel() 
    {
        return Excel::download(new ColorsExport, 'color.xlsx');
    }

    public function pdf()
    {
        $color = Color::all();
        $pdf = PDF::loadView('backend.colors.pdf', ['colors' => $color]);
        return $pdf->download('colors.pdf');
    }
}
