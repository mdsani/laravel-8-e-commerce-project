<?php

namespace App\Http\Controllers;

use App\Exports\BrandsExport;
use App\Models\Brand;
use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateBrandRequest;
use Illuminate\Database\QueryException;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class BrandController extends Controller
{
    public function index()
    {
        try{
            $brandCollection = Brand::latest();
            if(request('search')){
                $brand = $brandCollection
                                ->where('name','like', '%'.request('search').'%')
                                ->orWhere('description', 'like', '%'.request('search').'%');
            }
                $brand = $brandCollection->paginate(5);
                return view('backend.brands.index',[
                'brands' => $brand,
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.brands.create');
    }

    public function store(StoreBrandRequest $request)
    {
        try{
            Brand::create([
                'name' => $request->name,
                'description' =>$request->description
            ]);
            return redirect()->route('brands.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Brand $brand)
    {
        return view('backend.brands.show',[
            'brand' => $brand
        ]);
    }

    public function edit(Brand $brand)
    {
        return view('backend.brands.edit',[
            'brand' =>$brand
        ]);
    }

    public function update(UpdateBrandRequest $request, Brand $brand)
    {
        try{
            $brand->update([
                'name' => $request->name,
                'description' => $request->description
            ]);
            return redirect()->route('brands.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Brand $brand)
    {
        try{
            $brand->delete();
            return redirect()->route('brands.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $brand = Brand::onlyTrashed()->get();
            return view('backend.brands.trash',[
                'brands' => $brand
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $brand = Brand::onlyTrashed()->findOrFail($id);
            $brand->restore();
            return redirect()->route('brands.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $brand = Brand::onlyTrashed()->findOrFail($id);
            $brand->forceDelete();
            return redirect()->route('brands.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function excel() 
    {
        return Excel::download(new BrandsExport, 'brand.xlsx');
    }

    public function pdf()
    {
        $brand = Brand::all();
        $pdf = PDF::loadView('backend.brands.pdf', ['brands' => $brand]);
        return $pdf->download('brands.pdf');
    }
}
