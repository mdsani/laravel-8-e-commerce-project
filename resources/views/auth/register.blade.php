<x-guest-layout>
    <x-auth-card>

        <!-- Validation Errors -->
        <x-backend.layouts.elements.errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-backend.form.input name="name" :value="old('name')"/>
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-backend.form.input name="email" :value="old('email')"/>
            </div>

            <!-- Mobile Number -->
            <div class="mt-4">
                <x-backend.form.input name="mobile_number" type="number" :value="old('mobile_number')"/>
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-backend.form.input name="password" type="password" :value="old('password')"/>
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-backend.form.input name="password_confirmation" type="password" :value="old('password_confirmation')"/>
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900 btn btn-sm btn-primary" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-backend.form.button>Log in</x-backend.form.button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
