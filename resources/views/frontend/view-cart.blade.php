<x-frontend.layouts.master>

    <x-slot name="pagetitle">
        View Cart
    </x-slot>

    <div class="page-content pt-7 pb-10">
        <div class="step-by pr-4 pl-4">
            <h3 class="title title-simple title-step active">1. Shopping Cart</a></h3>
            <h3 class="title title-simple title-step">2. Checkout</a></h3>
        </div>
        <div class="container mt-7 mb-2">
            @if($cartItems->count() > 0)
                <div class="row">
                    <div class="col-lg-12 col-md-12 pr-lg-4">
                        <table class="shop-table cart-table">
                            <thead>
                                <tr>
                                    <th><span>Product</span></th>
                                    <th><span>Name</span></th>
                                    <th><span>Price</span></th>
                                    <th><span>quantity</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total = 0;
                                @endphp
                                @foreach ($cartItems as $item)                    
                                <tr class="product_data">
                                    <td class="product-thumbnail">
                                        <figure>
                                            <a href="#">
                                                <img src="{{ asset('storage/images/products/'.$item->products->image1) }}" width="100" height="100"
                                                    alt="product">
                                            </a>
                                        </figure>
                                    </td>
                                    <td class="product-name">
                                        <div class="product-name-section">
                                            <a href="#">{{ $item->products->name }}</a>
                                        </div>
                                    </td>
                                    <td class="product-price">
                                        <span class="amount">TK {{ $item->products->price }}</span>
                                    </td>
                                    <td>
                                        <input type="hidden" class="prod_id" value="{{ $item->prod_id }}">
                                        @if ($item->products->quantity >= $item->prod_qty)
                                        <div class="input-group texr-center" style="width: 130px">
                                            <span class="input-group-text changeQuantity decrement-btn mt-2">-</span>
                                            <input type="text" name="quantity" class="form-control text-center qty-input" value="{{ $item->prod_qty }}">
                                            <span class="input-group-text changeQuantity increment-btn mt-2">+</span>
                                        </div>
                                        @php
                                            $total +=$item->products->price * $item->prod_qty
                                        @endphp
                                        @else
                                        <div class="input-group texr-center" style="width: 130px">
                                            <h6>Out Of Stock</h6>
                                        </div>
                                         @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-danger delete-cart-item"> <i class="fa fa-trash"></i> Remove</button>
                                    </td>
                                </tr>
                                @endforeach
    
                            </tbody>
                        </table>
                        <h6>Total Price:{{ $total }}</h6>
                        <div class="cart-actions mb-6 pt-4">
                            <a href="{{ route('category-filter') }}" class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4"><i
                                    class="d-icon-arrow-left"></i>Continue Shopping</a>
                            <a href="{{ route('checkout') }}"
                                class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4">Process Checkout</a>
                        </div>
                        <div class="cart-coupon-box mb-8">
                            <h4 class="title coupon-title text-uppercase ls-m">Coupon Discount</h4>
                            <input type="text" name="coupon_code"
                                class="input-text form-control text-grey ls-m mb-4" id="coupon_code" value=""
                                placeholder="Enter coupon code here...">
                            <button type="submit" class="btn btn-md btn-dark btn-rounded btn-outline">Apply
                                Coupon</button>
                        </div>
                    </div>
                </div>
                @else
                    <div class="card-body text-center">
                         <h2>Your <i class="fa fa-shopping-cart"> Cart is empty</i></h2>
                    </div>
                    <div class="cart-actions mb-6 pt-4">
                        <a href="{{ route('category-filter') }}" class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4"><i
                                class="d-icon-arrow-left"></i>Continue Shopping</a>
                    </div>
            @endif
        </div>
    </div>

</x-frontend.layouts.master>