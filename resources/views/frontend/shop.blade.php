<x-frontend.layouts.master>

    <x-slot name="pagetitle">
        Shop
    </x-slot>

    <div class="page-content mb-10 pb-2">
        <div class="container">
            
            <div class="toolbox-wrap">
                <aside class="sidebar sidebar-fixed shop-sidebar closed">
                    <x-frontend.shop-sidebar/>
                </aside>
                <div class="toolbox sticky-content sticky-toolbox fix-top">
                    <x-frontend.shop-sticky-content/>
                </div>
            </div>

            <div class="row cols-2 cols-sm-3 cols-md-4 product-wrapper">
                {{-- @dd($products->all()) --}}
                @foreach ($products as $product)
                    {{-- @foreach ($product as $product) --}}
                        
                    <x-frontend.product-card :product="$product"/>
                    {{-- @endforeach --}}
                @endforeach
            </div>


            <nav class="toolbox toolbox-pagination">
                <ul class="pagination">
                    {{-- {{ $products->links() }} --}}
                </ul>
            </nav>
            
        </div>

    </div>

</x-frontend.layouts.master>