<x-frontend.layouts.master>

    <x-slot name="pagetitle">
        Profile
    </x-slot>

    <div class="page-content pt-7 pb-10">
        <div class="container mt-7 mb-2">
            <div class="card-body">
                <x-backend.layouts.elements.errors :errors="$errors"/>
                <x-backend.layouts.elements.message :message="session('message')"/>
                <form action="{{ route('user.profile.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card text-center sidebar p-4">
                                @if (isset($user->profile->image))
                                <img src="{{ asset('storage/images/profiles/'.$user->profile->image) }}" alt="" class="rounded-circle">
                                @else
                                <img src="{{ asset('ui/frontend/images/profile.png') }}" alt="" class="rounded-circle">
                                @endif
                                    <div class="col-md-12 mt-3 mb-3">
                                        <x-backend.form.input name="image" type="file"/>
                                    </div>
                                    <div class="mb-2">
                                        <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Save</button></div>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-8 p-2">
                            <div class="col-md-12">
                                <x-backend.form.input name="name" :value="$user->name"/>
                            </div>
                            <div class="col-md-12">
                                <x-backend.form.input name="email" type="email" :value="$user->email"/>
                            </div>
                            <div class="col-md-12">
                                <x-backend.form.input name="mobile_number" type="number" :value="old('mobile_number',$user->profile->mobile_number)"/>
                            </div>
                            <div class="col-md-12">
                                <label for="country">Country</label>
                                <select id="country" class="form-control" name="country">
                                    <option value="">Choose...</option>
                                    <option {{ old('country',$user->profile->country) == 'bangladesh' ? 'selected' : '' }} value="bangladesh">Bangladesh</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <x-backend.form.textarea name="address">
                                    {{ old('address',$user->profile->address) }}
                                </x-backend.form.textarea>
                            </div>
                            <div class="col-md-12">
                                <x-backend.form.textarea name="bio">
                                    {{ old('bio',$user->profile->bio) }}
                                </x-backend.form.textarea>
                            </div>
                            <div class="col-md-12">
                                <label for="city">City</label>
                                <select id="city" class="form-control" name="city">
                                    <option value="">Choose...</option>
                                    <option {{ old('city',$user->profile->city) == 'dhaka' ? 'selected' : '' }} value="dhaka">Dhaka</option>
                                    <option {{ old('city',$user->profile->city) == 'barisal' ? 'selected' : '' }} value="barisal">Barisal</option>
                                    <option {{ old('city',$user->profile->city) == 'khulna' ? 'selected' : '' }} value="khulna">Khulna</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="area">Area</label>
                                <select id="area" class="form-control" name="area">
                                    <option value="">Choose...</option>
                                    <option {{ old('area',$user->profile->area) == 'jhalokati' ? 'selected' : '' }} value="jhalokati">Jhalokati</option>
                                    <option {{ old('area',$user->profile->area) == 'nalcity' ? 'selected' : '' }} value="nalcity">Nalcity</option>
                                    <option {{ old('area',$user->profile->area) == 'uttara' ? 'selected' : '' }} value="uttara">Uttara</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label>Post Code</label>
                                <x-backend.form.input name="postcode" type="number" :value="$user->profile->postcode"/>
                            </div>  
                        </div>
                                     
                    </div>

            </div>
        </div>
    </div>

</x-frontend.layouts.master>