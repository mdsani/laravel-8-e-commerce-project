<x-frontend.layouts.master>

    <x-slot name="pagetitle">
        My Order
    </x-slot>

    <div class="page-content pt-7 pb-10">
        <div class="step-by pr-4 pl-4">
            <h4>My Order</h4>
        </div>
        <div class="container mt-7 mb-2">
                <div class="row">
                    <div class="col-lg-12 col-md-12 pr-lg-4">
                        <table class="shop-table cart-table">
                            <thead>
                                <tr>
                                    <tr>
                                        <th>Order Date</th>
                                        <th>Tracking Number</th>
                                        <th>Total Price</th>
                                        <th>Status</th>
                                        <th class="d-flex justify-content-end">Action</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $item)
                                    <tr>
                                        <td>{{ date('d-m-y', strtotime($item->created_at)) }}</td>
                                        <td>{{ $item->tracking_no }}</td>
                                        <td>{{ $item->total_price }}</td>
                                        <td>{{ $item->status == '0' ? 'Pending' : ($item->status == '1' ? 'Shipping' :($item->status == '2' ? 'Complate': 'Cancel')) }}</td>
                                        <td class="d-flex justify-content-end"><a href="{{ route('view-order', ['id' => $item->id]) }}"  class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4">View</a></td>
                                    </tr>
                                @endforeach
    
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
    

</x-frontend.layouts.master>