<x-frontend.layouts.master>

    <x-slot name="pagetitle">
        Shop Classic Filter
    </x-slot>

<div class="page-header" style="background-image: url('{{ asset('ui/frontend/images/shop/page-header-back.jpg') }}'); background-color: #3C63A4;">
    <x-frontend.page-header/>
</div>
<!-- End PageHeader -->
<div class="page-content mb-10 pb-6">
    <div class="container">
        <div class="row gutter-lg main-content-wrap">
            <aside class="col-lg-3 sidebar sidebar-fixed sidebar-toggle-remain shop-sidebar sticky-sidebar-wrapper">
                
                <div class="sidebar-overlay"></div>
                <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                <a href="#" class="sidebar-toggle">
                    <i class="fas fa-chevron-right"></i>
                </a>

                <div class="sidebar-content">
                    <div class="sticky-sidebar" data-sticky-options="{'top': 10}">

                        <div class="widget widget-collapsible">
                            <x-frontend.shop-classic-all-category :categories="$categories"/>
                        </div>

                        <div class="widget widget-collapsible">
                            <x-frontend.shop-classic-price/>
                        </div>
                        
                        <div class="widget widget-collapsible">
                            <x-frontend.shop-classic-size :sizes="$sizes"/>
                        </div>

                        <div class="widget widget-collapsible">
                            <x-frontend.shop-classic-color :colors="$colors"/>
                        </div>

                        <div class="widget widget-collapsible">
                            <x-frontend.shop-classic-brand :brands="$brands"/>
                        </div>

                    </div>
                </div>

            </aside>

            <div class="col-lg-9 main-content">
                
                <x-frontend.shop-classic-nav/>

                <div class="row cols-2 cols-sm-3 product-wrapper">
                    
                    @foreach ($products as $product)
                       <x-frontend.product-card :product="$product"/>
                   @endforeach

                </div>

                <nav class="toolbox toolbox-pagination">
                    <ul class="pagination">
                        {{ $products->links() }}
                    </ul>
                </nav>
            </div>
            
        </div>
    </div>
</div>

</x-frontend.layouts.master>