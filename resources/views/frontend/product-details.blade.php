<x-frontend.layouts.master>
    <x-slot name="pagetitle">
        Product Details
    </x-slot>

    <div class="page-content mb-10 pb-6">
        <div class="container">
            {{-- @dd($commentCount); --}}
            <x-frontend.single-product :product="$product"/>

            <div class="tab tab-nav-simple product-tabs mb-4 container">
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#product-tab-description">Description</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#product-tab-additional">Additional information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#product-tab-reviews">Reviews</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active in mb-3" id="product-tab-description">
                        <x-frontend.product-tab-description :product="$product"/>
                    </div>
                    <div class="tab-pane" id="product-tab-additional">
                        <x-frontend.product-tab-additional :product="$product"/>
                    </div>
                    <div class="tab-pane " id="product-tab-reviews">
                        <x-frontend.product-tab-reviews :product="$product" :comments="$comments"/>
                    </div>
                </div>
            </div>

            <section class="mt-10 container">
                <h2 class="title title-center ls-normal">Related Products</h2>

                <div class="owl-carousel owl-theme owl-nav-full row cols-2 cols-md-3 cols-lg-4"
                    data-owl-options="{
                    'items': 5,
                    'nav': false,
                    'loop': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '992': {
                            'items': 4,
                            'dots': false,
                            'nav': true
                        }
                    }
                }">
                    @foreach ($productrelated as $product)
                    <x-frontend.product-card :product="$product"/>
                    @endforeach
                    
                </div>
            </section>
        </div>
    </div>
    
</x-frontend.layouts.master>    