<h3 class="widget-title">Size</h3>
<ul class="widget-body filter-items">
    @foreach ($sizes as $key=>$value)
    <li><a href="#">{{ $value }}</a></li>
    @endforeach
</ul>