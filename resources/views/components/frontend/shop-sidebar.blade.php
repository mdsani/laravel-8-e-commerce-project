<div class="sidebar-overlay"></div>
<a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
<div class="sidebar-content">
    <div class="mb-0 mb-lg-4">
        <div class="filter-actions">
            <a href="#"
                class="sidebar-toggle-btn toggle-remain btn btn-sm btn-outline btn-primary btn-rounded">Filter<i
                    class="d-icon-arrow-left"></i></a>
            <a href="#" class="filter-clean text-primary">Clean All</a>
        </div>
        <div class="row cols-lg-4">
            <div class="widget">
                <h3 class="widget-title">Size</h3>
                <ul class="widget-body filter-items">
                    <li><a href="#">Extra Large</a></li>
                    <li><a href="#">Large</a></li>
                    <li><a href="#">Medium</a></li>
                    <li><a href="#">Small</a></li>
                </ul>
            </div>
            <div class="widget">
                <h3 class="widget-title">Color</h3>
                <ul class="widget-body filter-items">
                    <li><a href="#">Black</a></li>
                    <li><a href="#">Blue</a></li>
                    <li><a href="#">Brown</a></li>
                    <li><a href="#">Green</a></li>
                </ul>
            </div>
            <div class="widget">
                <h3 class="widget-title">Brand</h3>
                <ul class="widget-body filter-items">
                    <li><a href="#">Cinderella</a></li>
                    <li><a href="#">Comedy</a></li>
                    <li><a href="#">SkillStar</a></li>
                    <li><a href="#">SLS</a></li>
                </ul>
            </div>
            <div class="widget price-with-count filter-price">
                <h3 class="widget-title">Price</h3>
                <ul class="widget-body filter-items">
                    <li class="active"><a href="#">All</a><span>(10)</span></li>
                    <li><a href="#">$0.00 - $100.00</a><span>(1)</span></li>
                    <li><a href="#">$100.00 - $200.00</a><span>(9)</span></li>
                    <li><a href="#">$200.00+</a><span>(3)</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>