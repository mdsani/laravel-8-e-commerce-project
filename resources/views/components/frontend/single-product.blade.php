@props(['product'])
<div class="product product-single row mb-2">
    <div class="col-md-6">
        <div class="product-gallery pg-vertical product-gallery-sticky">
            <div class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1">
                <figure class="product-image">
                    <img src="{{ asset('storage/images/products/'.$product->image1) }}"
                        data-zoom-image="{{ asset('storage/images/products/'.$product->image1) }}"
                        alt="Blue Pinafore Denim Dress" width="800" height="900"
                        style="background-color: #f2f3f5;" />
                </figure>
                <figure class="product-image">
                    <img src="{{ asset('storage/images/products/'.$product->image2) }}"
                        data-zoom-image="{{ asset('storage/images/products/'.$product->image2) }}"
                        alt="Blue Pinafore Denim Dress" width="800" height="900"
                        style="background-color: #f2f3f5;" />
                </figure>
                <figure class="product-image">
                    <img src="{{ asset('storage/images/products/'.$product->image3) }}"
                        data-zoom-image="#"
                        alt="Blue Pinafore Denim Dress" width="800" height="900"
                        style="background-color: #f2f3f5;" />
                </figure>
                <figure class="product-image">
                    <img src="{{ asset('storage/images/products/'.$product->image4) }}"
                        data-zoom-image="{{ asset('ui/frontend/images/demos/demo2/product/product-4-800x900.jpg') }}"
                        alt="Blue Pinafore Denim Dress" width="800" height="900"
                        style="background-color: #f2f3f5;" />
                </figure>
            </div>
            <div class="product-thumbs-wrap">
                <div class="product-thumbs">
                    <div class="product-thumb active">
                        <img src="{{ asset('storage/images/products/'.$product->image1) }}"
                            alt="product thumbnail" width="109" height="122"
                            style="background-color: #f2f3f5;" />
                    </div>
                    <div class="product-thumb">
                        <img src="{{ asset('storage/images/products/'.$product->image2) }}"
                            alt="product thumbnail" width="109" height="122"
                            style="background-color: #f2f3f5;" />
                    </div>
                    <div class="product-thumb">
                        <img src="{{ asset('storage/images/products/'.$product->image3) }}"
                            alt="product thumbnail" width="109" height="122"
                            style="background-color: #f2f3f5;" />
                    </div>
                    <div class="product-thumb">
                        <img src="{{ asset('storage/images/products/'.$product->image4) }}"
                            alt="product thumbnail" width="109" height="122"
                            style="background-color: #f2f3f5;" />
                    </div>
                </div>
                <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i></button>
                <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i></button>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="product-details product_data mt-3">
            <div class="product-navigation">
                    <a href="{{ route('home') }}"><i class="d-icon-home"> Products</i></a>
                </ul>
            </div>

            <h1 class="product-name">{{ $product->name ?? '' }}</h1>
            <div class="product-meta">
                SKU: <span class="product-sku">{{ $product->sku }}</span>
                @if (!is_null($product->brand_id))
                BRAND: <span class="product-brand">{{ $product->brand->name ?? '' }}</span>
                @endif
            </div>
            <div class="product-price mb-2">TK{{ $product->price }}</div>
            <div class="ratings-container">
                <div class="ratings-full">
                    <span class="ratings" style="width:80%"></span>
                    <span class="tooltiptext tooltip-top"></span>
                </div>
                <a href="#product-tab-reviews" class="link-to-tab rating-reviews">( 6 reviews )</a>
            </div>
            <p class="product-short-desc">{{ $product->short_description }}</p>

            @if ($product->quantity > 0)
                <label for="" class="badge bg-success" style="height:">In Stock</label>
            @else
                <label for="" class="badge bg-danger">Out Of Stock</label>
            @endif

            <hr class="product-divider">

            <div class="product-form product-qty">
                @if (isset($product->colors))
                <div class="input-group mr-4">
                    <select name="color" id="prod_color">
                        <option value="">Color..</option>
                        @foreach ($product->colors as $color)
                            <option value="{{ $color->id }}">{{ $color->name }}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                {{-- @if (isset($product->sizes))
                <div class="input-group mr-4">
                    <select name="color" id="prod_color">
                        <option value="">Color..</option>
                        @foreach ($product->sizes as $size)
                            <option value="{{ $size->id }}">{{ $size->name }}</option>
                        @endforeach
                    </select>
                </div>
                @endif --}}
                <div class="product-form product-qty ml-4">
                    <div class="product-form-group">
                        <input type="hidden" value="{{ $product->id }}" class="prod_id">
                        <div class="input-group texr-center" style="width: 130px">
                            <button class="input-group-text decrement-btn">-</button>
                            <input type="text" name="quantity" class="form-control text-center qty-input" value="1">
                            <button class="input-group-text increment-btn">+</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-form">
                <div class="product-form-group col-md-4">
                    <button class="wish-btn addToWishlistBtn text-normal ls-normal font-weight-semi-bold"><i
                        class="d-icon-heart"></i>Add to
                    wishlist</button>    
                </div>
                <div class="product-form-group col-md-4">
                    @if ($product->quantity > 0 && $product->featured == 0)
                    <button class="buy-btn  addToCartBtn text-normal ls-normal font-weight-semi-bold">
                        <i class="d-icon-bag"></i>Add to Cart
                    </button>
                    @endif
                        
                </div>
            </div>
{{--         

            <hr class="product-divider mb-3">
            <div class="product-footer">
                <div class="social-links mr-4">
                    <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                    <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                    <a href="#" class="social-link social-pinterest fab fa-pinterest-p"></a>
                </div>
                <span class="divider d-lg-show"></span>
                <div class="product-action">
                    
                    <a href="#" class="btn-product btn-compare"><i class="d-icon-compare"></i>Add to
                        compare</a>
                </div>
            </div>
        </div> --}}
        <div class="rating-css">
            <div class="star-icon">
                <input type="radio" value="1" name="product_rating" checked id="rating1">
                <label for="rating1" class="fa fa-star"></label>
                <input type="radio" value="2" name="product_rating" id="rating2">
                <label for="rating2" class="fa fa-star"></label>
                <input type="radio" value="3" name="product_rating" id="rating3">
                <label for="rating3" class="fa fa-star"></label>
                <input type="radio" value="4" name="product_rating" id="rating4">
                <label for="rating4" class="fa fa-star"></label>
                <input type="radio" value="5" name="product_rating" id="rating5">
                <label for="rating5" class="fa fa-star"></label>
            </div>
        </div>
    </div>



</div>