<div class="banner-content w-100 pl-3 text-right text-uppercase">
    <h4 class="banner-subtitle font-weight-bold text-uppercase mb-2">24 Hours Only</h4>
    <h1 class="banner-title mb-3 text-uppercase font-weight-normal">Flash <strong>Sale</strong>
    </h1>
    <div class="banner-price-info mb-3 text-dark"><strong>Up to 70</strong>% Discount</div>
    <p class="d-inline-block mb-6 font-primary text-secondary text-uppercase bg-dark">
        Promocode:<strong class="text-normal"> Riode 20</strong></p><br />
    <a href="#" class="btn btn-link btn-underline">Shop now<i
            class="d-icon-arrow-right"></i></a>
</div>