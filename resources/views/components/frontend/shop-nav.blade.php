<nav class="breadcrumb-nav">
    <ul class="breadcrumb breadcrumb-lg">
        <li><a href="{{ route('home') }}"><i class="d-icon-home"></i></a></li>
        <li>Categories</li>
    </ul>
</nav>