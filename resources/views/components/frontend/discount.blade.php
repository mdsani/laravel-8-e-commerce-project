<div class="banner-content appear-animate"
    data-animation-options="{'name': 'fadeInUpShorter','duration': '1s'}">
    <h4 class="banner-subtitle text-uppercase text-white font-weight-normal lh-1 ls-m mb-0">End
        Of Season</h4>
    <hr class="divider mb-2">
    <h3 class="banner-title text-uppercase text-white lh-1 mb-0">Sale</h3>
    <div class="price-rotated">
        <div class="banner-price-info d-flex align-items-center justify-content-center skrollr"
            data-options='{"data-bottom-top":"transform: translate(0, 0) scale(1.6); transform-origin: center;", "data-center":"transform: translate(0, 0) scale(1);"}'
            data-animation-options="{'name': 'fadeIn', 'duration': '1.2s', 'delay': '.2s'}">
            <h5 class="text-uppercase text-white ls-l mb-0">At Up To<br><span
                    class="text-secondary ls-l">50%</span> Off</h5>
        </div>
    </div>
</div>