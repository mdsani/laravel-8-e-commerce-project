@props(['product'])
<div class="product appear-animate" data-animation-options="{
    'name': 'fadeInLeftShorter',
    'delay': '.4s'
}">
    <figure class="product-media">
        <a href="{{ route('product-details',['product'=>$product->id]) }}">
            <img src="{{ asset('storage/images/products/'.$product->image1) }}" alt="Blue Pinafore Denim Dress"
                width="280" height="315" style="background-color: #f2f3f5;" />
        </a>
        {{-- <div class="product-label-group">
            <label class="product-label label-new">new</label>
        </div> --}}
        {{-- <div class="product-action-vertical">
            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                data-target="#addCartModal" title="Add to cart"><i
                    class="d-icon-bag"></i></a>
            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                    class="d-icon-heart"></i></a>
        </div> --}}
        <div class="product-action">
            <a href="{{ route('product-details',['product'=>$product->id]) }}" class="btn-product btn-quickview" title="Quick View">Quick
                View</a>
        </div>
    </figure>
    <div class="product-details">
        <div class="product-cat">
            <a href="{{ route('product-details',['product'=>$product->id]) }}">{{ $product->category->name ?? '' }}</a>
        </div>
        <h3 class="product-name">
            <a href="{{ route('product-details',['product'=>$product->id]) }}">{{  $product->name }}</a>
        </h3>
        <div class="product-price">
            <span class="price">${{ $product->price }}</span>
        </div>
        <div class="ratings-container">
            <div class="ratings-full">
                <span class="ratings" style="width:100%"></span>
                <span class="tooltiptext tooltip-top"></span>
            </div>
            <a href="{{ route('product-details',['product'=>$product->id]) }}" class="rating-reviews">( 12 reviews )</a>
        </div>
    </div>
</div>


















{{-- <div class="product appear-animate" data-animation-options="{
    'name': 'fadeInLeftShorter',
    'delay': '.4s'
}">
    <figure class="product-media">
        <a href="{{ route('productdetails') }}">
            <img src="{{ asset('storage/images/products/'.$product->image1) }}" alt="Blue Pinafore Denim Dress"
                width="280" height="315" style="background-color: #f2f3f5;" />
        </a>
        <div class="product-label-group">
            <label class="product-label label-new">new</label>
        </div>
        <div class="product-action-vertical">
            <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                data-target="#addCartModal" title="Add to cart"><i
                    class="d-icon-bag"></i></a>
            <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                    class="d-icon-heart"></i></a>
        </div>
        <div class="product-action">
            <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                View</a>
        </div>
    </figure>
    <div class="product-details">
        <div class="product-cat">
            <a href="demo2-shop.html">{{ $product->category->name }}</a>
        </div>
        <h3 class="product-name">
            <a href="demo2-product.html">Comfortable Brown Scart</a>
        </h3>
        <div class="product-price">
            <span class="price">${{ $product->price }}</span>
        </div>
        <div class="ratings-container">
            <div class="ratings-full">
                <span class="ratings" style="width:100%"></span>
                <span class="tooltiptext tooltip-top"></span>
            </div>
            <a href="demo2-product.html" class="rating-reviews">( 12 reviews )</a>
        </div>
    </div>
</div> --}}
