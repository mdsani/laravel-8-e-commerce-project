@props(['product'])
<ul class="list-none">
    @if (isset($product->colors))      
        <li>
            <label>Color:</label>
            @foreach ($product->colors as $color)
                <p>{{ $color->name }} ,</p>
            @endforeach
        </li>
    @endif
    @if(isset($product->sizes))
        <li>
            <label>Size:</label>
            @foreach ($product->sizes as $size)
            <p>{{ $size->title }} ,</p>
            @endforeach
        </li>
    @endif
</ul>