
<div class="header-bottom d-lg-show" style="background-color: #11A0DB;">
    <div class="container">
        <div class="header-left">
            <nav class="main-nav">
                <ul class="menu menu-active-underline text-white">
                    <li class="{{ request()->is('home') ? 'active' : '' }}">
                        <a href="{{ route('home') }}">Home</a>
                    </li>
                    {{-- <li>
                        <a href="#">SmartPhones</a>
                        <ul>
                            <li><a href="{{ route('category-filter') }}"><h4 class="menu-title">Classic Filter</h4></a></li>
                            @foreach ($categories as $key=>$value)
                            <li><a href="{{ route('categories.shop',['category'=> $key]) }}">{{ $value }}</a></li>
                            @endforeach
                        </ul>
                    </li> --}}
                    {{-- @dd($categories->subcategory->all()) --}}
                    @foreach ($categories as $category)
                    <li>
                        <a href="#">{{ $category->name }}</a>
                        <ul>
                            @foreach ($category->subcategory as $subcategory)
                            <li><a href="{{ route('sub-categories.shop',['sub_category'=>$subcategory->id]) }}">{{ $subcategory->name ?? '' }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </nav>
        </div>
    </div>
</div>