<div class="header-middle sticky-header fix-top sticky-content">
    <div class="container">
        <div class="header-left">
            <a href="#" class="mobile-menu-toggle">
                <i class="d-icon-bars2"></i>
            </a>
            <a href="demo2.html" class="logo">
                <img src="{{ asset('ui/frontend/images/demos/demo2/logo.png') }}" alt="logo" width="153" height="44" />
            </a>
            <!-- End Logo -->

            <div class="header-search hs-simple">
                <form action="#" class="input-wrapper">
                    <input type="text" class="form-control" name="search" autocomplete="off"
                        placeholder="Search..." required />
                    <button class="btn btn-search" type="submit" title="submit-button">
                        <i class="d-icon-search"></i>
                    </button>
                </form>
            </div>
            <!-- End Header Search -->
        </div>
        <div class="header-right">
            <!--Wishlist Start-->
            <div class="dropdown cart-dropdown type2 mr-0 mr-lg-2">
                <a href="{{ route('view.wishlist') }}" class="wishlist-toggle" title="wishlist">
                    <i class="d-icon-heart"><span class="cart-count">{{ $shoopingWishCount }}</span></i>
                </a>
            </div>
            <!--Wishlist End-->

            <span class="divider"></span>
            <div class="dropdown cart-dropdown type2 mr-0 mr-lg-2">
                <a href="{{ route('view.cart') }}" class="cart-toggle label-block link">
                    <div class="cart-label d-lg-show">
                        <span class="cart-name">Shopping Cart:</span>
                    </div>
                    <i class="d-icon-bag"><span class="cart-count">{{ $shoopingCartCount }}</span></i>
                </a>
                <!-- End Dropdown Box -->
            </div>
            <span class="divider"></span>
            <ul class="menu menu-active-underline">
                <li>
                    <a href="#"><i class="fa fa-user"></i> {{ auth()->user()->name ?? 'Profile' }}</a>
                    <ul>
                        <li><a class="dropdown-item" href="{{ route('profile.edit') }}">Profile</a></li>
                        <li><a class="dropdown-item" href="{{ route('my-order') }}">View Order</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li>
                            <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a class="dropdown-item" onclick="event.preventDefault(); this.closest('form').submit();">Logout</a>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
            <span class="divider"></span>
                <a href="{{ route('login') }}" data-toggle="login-modal"><i class="d-icon-user"></i>Sign in</a>
                <span class="delimiter">/</span>
                <a class=" ml-0" href="{{ route('register') }}" data-toggle="login-modal">Register</a>
        </div>
    </div>

</div>