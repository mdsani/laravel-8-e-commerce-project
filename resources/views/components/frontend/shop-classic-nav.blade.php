<nav class="toolbox  sticky-toolbox sticky-content fix-top">
    <div class="toolbox-left">
        <div class="toolbox-item toolbox-sort select-box text-dark">
            <label>Sort By :</label>
            <select name="orderby" class="form-control">
                <option value="default">Default</option>
                <option value="popularity" selected="selected">Most Popular</option>
                <option value="rating">Average rating</option>
                <option value="date">Latest</option>
                <option value="price-low">Sort forward price low</option>
                <option value="price-high">Sort forward price high</option>
                <option value="">Clear custom sort</option>
            </select>
        </div>
    </div>
    <div class="toolbox-right">
        <div class="toolbox-item toolbox-show select-box text-dark">
            <label>Show :</label>
            <select name="count" class="form-control">
                <option value="12">12</option>
                <option value="24">24</option>
                <option value="36">36</option>
            </select>
        </div>
        <div class="toolbox-item toolbox-layout mr-lg-0">
            <a href="shop-list-mode.html" class="d-icon-mode-list btn-layout"></a>
            <a href="shop.html" class="d-icon-mode-grid btn-layout active"></a>
        </div>
    </div>
</nav>