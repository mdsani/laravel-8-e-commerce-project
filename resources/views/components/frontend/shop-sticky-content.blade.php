<div class="toolbox-left">
    <a href="#"
        class="toolbox-item left-sidebar-toggle btn btn-outline btn-primary btn-icon-left font-primary btn-rounded"><i
            class="d-icon-filter-2"></i>Filter</a>
</div>
<div class="toolbox-right">
    <div class="toolbox-item toolbox-sort select-box">
        <label>Sort By :</label>
        <select name="orderby" class="form-control">
            <option value="default" selected="selected">Default sorting</option>
            <option value="popularity">Sort by popularity</option>
            <option value="rating">Sprt by average rating</option>
            <option value="date">Sort by latest</option>
            <option value="price-low">Sort by price: low to high</option>
            <option value="price-high">Sort by price: high to low</option>
        </select>
    </div>
    <div class="toolbox-item toolbox-layout">
        <a href="shop-list-mode.html" class="d-icon-mode-list btn-layout"></a>
        <a href="shop.html" class="d-icon-mode-grid btn-layout active"></a>
    </div>
</div>