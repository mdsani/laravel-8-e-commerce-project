<h3 class="widget-title">Brands</h3>
<ul class="widget-body filter-items">
    @foreach ($brands as $key=>$value)
    <li><a href="#">{{ $value }}</a></li>
    @endforeach
</ul>