<x-backend.layouts.master>
    <x-slot name="page_title">
        Notifications
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Notifications
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Notifications</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Notifications</span>
        </div>
    </div>
    <div class="card-body">
        @foreach ($notifications as $key=>$value)
            <p>{{ $key }} : {{ $value }}</p>
        @endforeach
    </div>
</div>

</x-backend.layouts.master>



