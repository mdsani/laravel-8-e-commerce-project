<x-backend.layouts.master>
    <x-slot name="page_title">
        Tags
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Tags
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Tags</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table"></i>Tags</span>
            @canany(['admin','editor'])
                <span>
                    <a class="btn btn-sm btn-primary text-left" href="{{ Route('tags.create') }}" role="button">Add</a>
                    <a class="btn btn-sm btn-danger text-left" href="{{ route('tags.trash') }}" role="button">Trash</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('tags.excel') }}" role="button">Excel</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('tags.pdf') }}" role="button">Pdf</a>
                </span>
            @endcanany
        </div>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-end">
            <form action="{{ route('tags.index') }}" method="GET">
                <x-backend.form.search name="search" placeholder="Search" style="width: 220px;"/>
            </form>
        </div>
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($tags as $tag)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $tag->title }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('tags.show', ['tag'=>$tag->id]) }}" role="button">Show</a>
                            @canany(['admin','editor'])
                                <a class="btn btn-sm btn-warning" href="{{ route('tags.edit', ['tag'=>$tag->id]) }}" role="button">Edit</a>
                                <form style="display: inline;" action="{{ route('tags.destroy', ['tag'=>$tag->id]) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            @endcanany
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



