<x-backend.layouts.master>
    <x-slot name="page_title">
        Tags
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Tags
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item">Tags</li>
                <li class="breadcrumb-item active">Create</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Tags</span>
                <span>
                    <a class="btn btn-sm btn-primary text-left" href="{{ Route('tags.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('tags.store') }}" method="POST">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-12">
                            <x-backend.form.input name="title" :value="old('title')"/>
                    </div>
                </div>

                <div class="mt-4 mb-0">
                    <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Save</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>