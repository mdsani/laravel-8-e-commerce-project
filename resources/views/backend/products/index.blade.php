<x-backend.layouts.master>
    <x-slot name="page_title">
        Products
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Products</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Products</span>
            @canany(['admin','editor'])
                <span>
                    <a class="btn btn-sm btn-primary text-left" href="{{ Route('products.create') }}" role="button">Add</a>
                    <a class="btn btn-sm btn-danger text-left" href="{{ route('products.trash') }}" role="button">Trash</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('products.excel') }}" role="button">Excel</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('products.pdf') }}" role="button">Pdf</a>
                </span>
            @endcanany
        </div>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-end mb-1">
            <form action="{{ route('products.index') }}" method="GET">
                <select name="category" class="form-select " style="width:220px">
                    <option value=""  style="border: 1px solid #DCDCDC;">Select One</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}"{{ $category->id==$req?'selected':'' }}>{{ $category->name }}</option>
                            @endforeach
                </select>
                <x-backend.form.button >
                    Submit
                  </x-backend.form.button>
            </form>
            <form action="{{ route('products.index') }}" method="GET">
                <x-backend.form.search name="search" placeholder="Search" style="width: 220px;"/>
            </form>
        </div>
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Sub Category Name</th>
                    <th>Our Feature Product</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($products as $product)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $product->subcategory->name }}</td>
                        <td>{{ $product->featured }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->price }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('products.show', ['product'=>$product->id]) }}" role="button">Show</a>
                            @canany(['admin','editor'])
                                <a class="btn btn-sm btn-warning" href="{{ route('products.edit', ['product'=>$product->id]) }}" role="button">Edit</a>
                                <form style="display: inline;" action="{{ route('products.destroy', ['product'=>$product->id]) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            @endcanany
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>
</div>

</x-backend.layouts.master>



