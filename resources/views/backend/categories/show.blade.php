<x-backend.layouts.master>
    <x-slot name="page_title">
        Categories
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Categories
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Categories</a></li>
            <li class="breadcrumb-item active">Show</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Categories</span>
            <span>
                <a class="btn btn-primary text-left" href="{{ Route('categories.index') }}" role="button">List</a>
            </span>
        </div>
    </div>
    <div class="card-body">
        <table class="table border">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
          foreach
            </tbody>      @foreach ($category->subcategory as $category)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $category->categories->name }}</td>
                    <td>{{ $category->name }}</td>
                </tr>
                @end
        </table>
    </div>
</div>

</x-backend.layouts.master>



